# -*- coding: utf-8 -*-
import argparse
import contextlib
import logging
from datetime import timedelta

from conf.parser import parse_config
from conf.providers import url_provider, file_provider, cached_data, timed_memory_store, retrying_wrapper
from proxy.handlers import InfoHandler, TorrentPatchHandler
from proxy.proxy import TCP, Proxy
from proxy.selecting_server import build_selecting_server_cls


# url 'https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_best.txt'
# GET http://x-tor.org/download/668088 HTTP/1.1 Direct -> HTTP/1.1 200 OK


def main():
    parser = argparse.ArgumentParser('Local Proxy')

    config_group = parser.add_mutually_exclusive_group()
    config_group.add_argument("-file", help="proxy config file location")
    config_group.add_argument("-url", help="proxy config file URL")

    tracker_group = parser.add_mutually_exclusive_group()
    tracker_group.add_argument("--trackers-file", help="Torrent trackers list file")
    tracker_group.add_argument("--trackers-url", help="Torrent trackers list URL")

    parser.add_argument("--v", "--verbose", default='ERROR', choices=['ERROR', 'INFO', 'DEBUG'])
    parser.add_argument("--cache-time", type=int, default=60,
                        help="Proxy config/Torrent trackers content caching time in seconds")
    parser.add_argument("--source-retry", type=int, default=3,
                        help="Proxy config/Torrent trackers content maximal obtaining retries")

    parser.add_argument("port", type=int, default=8080, help="proxy serving port")

    arguments = parser.parse_args()

    config_text_provider = None
    config_uri = None

    @contextlib.contextmanager
    def empty_announcers_provider():
        yield []

    announce_listener = None
    announcers_provider = empty_announcers_provider
    announcers_text_provider = None
    announcers_url = None

    maximal_read_retry_count = arguments.source_retry
    expected_read_errors = [IOError]
    cache_time = timedelta(seconds=arguments.cache_time)

    @contextlib.contextmanager
    def config_provider():
        with config_text_provider(config_uri) as config_content:
            yield parse_config(config_content)

    if arguments.file:
        config_text_provider = file_provider
        config_uri = arguments.file
    elif arguments.url:
        config_text_provider = url_provider
        config_uri = arguments.url
    config_provider = retrying_wrapper(config_provider, maximal_read_retry_count, expected_read_errors)
    config_provider = cached_data(config_provider, timed_memory_store(cache_time))

    if arguments.trackers_file:
        announcers_text_provider = file_provider
        announcers_url = arguments.trackers_file
    elif arguments.trackers_url:
        announcers_text_provider = url_provider
        announcers_url = arguments.trackers_url

    if announcers_text_provider:
        # 'https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all_http.txt'
        @contextlib.contextmanager
        def real_announcers_provider():
            with announcers_text_provider(announcers_url) as content:
                yield content.split()

        announcers_provider = real_announcers_provider
        announcers_provider = retrying_wrapper(announcers_provider, maximal_read_retry_count, expected_read_errors)
        announcers_provider = cached_data(announcers_provider, timed_memory_store(cache_time))

    class HTTP2(TCP):
        def __init__(self, hostname='127.0.0.1', port=8899, backlog=100,
                     auth_code=None, server_recvbuf_size=8192, client_recvbuf_size=8192):
            """HTTP proxy server implementation. Spawns new process to proxy accepted client connection."""
            super(HTTP2, self).__init__(hostname, port, backlog)
            self.auth_code = auth_code
            self.client_recvbuf_size = client_recvbuf_size
            self.server_recvbuf_size = server_recvbuf_size
            self.selecting_server_cls = build_selecting_server_cls(config_provider)

        def handle(self, client):
            proxy_instance = Proxy(client,
                                   server_cls=self.selecting_server_cls,
                                   http_handlers=[InfoHandler(config_provider),
                                                  TorrentPatchHandler(announcers_provider, announce_listener)],
                                   server_recvbuf_size=self.server_recvbuf_size,
                                   client_recvbuf_size=self.client_recvbuf_size)
            proxy_instance.daemon = True
            proxy_instance.start()

    logging.basicConfig(level=getattr(logging, arguments.v),
                        format='%(asctime)s - %(levelname)s - %(funcName)s:%(lineno)d - %(message)s')

    proxy = HTTP2(hostname='0.0.0.0',
                  port=arguments.port, )
    proxy.run()


if __name__ == '__main__':
    main()
