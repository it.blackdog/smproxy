# -*- coding: utf-8 -*-
import abc
import gzip
import zlib
from StringIO import StringIO

from proxy import HttpParser


class BaseHttpHandler(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def can_handle_response(self, response):
        pass

    def handle_response(self, response):
        pass

    @abc.abstractmethod
    def can_handle_request(self, request):
        pass

    def handle_request(self, request):
        pass

    @abc.abstractmethod
    def can_observe_flow(self, request):
        pass

    def observe_request(self, request_id, request):
        pass

    def observe_response(self, request_id, response):
        pass

    @staticmethod
    def extract_encoding(response):
        encoding_header = response.headers.get(b'content-encoding', (b'content-encoding', 'identity'))
        return encoding_header[1]

    @staticmethod
    def is_encoding_supported(encoding):
        return encoding in ['identity', 'gzip', 'x-gzip', 'deflate']

    @staticmethod
    def decode_content_body(body, encoding):
        if encoding == 'identity':
            text = body
        elif encoding in ('gzip', 'x-gzip'):
            io = StringIO(body)
            with gzip.GzipFile(fileobj=io) as f:
                text = f.read()
        elif encoding == 'deflate':
            try:
                text = zlib.decompress(body)
            except zlib.error:
                text = zlib.decompress(body, -zlib.MAX_WBITS)
        else:
            raise Exception("Unknown Content-Encoding: %s" % encoding)
        return text

    @staticmethod
    def encode_content_body(text, encoding):
        if encoding == 'identity':
            data = text
        elif encoding in ('gzip', 'x-gzip'):
            io = StringIO()
            with gzip.GzipFile(fileobj=io, mode='wb') as f:
                f.write(text)
            data = io.getvalue()
        elif encoding == 'deflate':
            data = zlib.compress(text)
        else:
            raise Exception("Unknown Content-Encoding: %s" % encoding)
        return data


def response_to_message(response):
    builder = HttpParser(HttpParser.types.RESPONSE_PARSER)
    builder.version = 'HTTP/1.1'
    builder.code = response.code
    builder.reason = 'OK'
    builder.state = HttpParser.states.COMPLETE

    builder.headers = {}
    for k, v in response.headers.iteritems():
        builder.headers[k] = (k, v)

    builder.body = response.body

    return builder
