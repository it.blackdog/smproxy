# -*- coding: utf-8 -*-
from collections import namedtuple

RESPONSE = namedtuple('RESPONSE', ['code', 'headers', 'body'])


def success_response(headers, body):
    return RESPONSE(200, headers, body)


def text_response(text):
    handled_headers = {}
    handled_headers['Content-Type'] = 'text/plain; charset=utf-8'
    handled_headers['Content-length'] = str(len(text))
    return success_response(handled_headers, text)


def file_response(name, content, content_type='application/octet-stream'):
    handled_headers = {}
    handled_headers['Content-Type'] = content_type
    handled_headers['Content-Disposition'] = 'attachment; filename="%s"' % name
    handled_headers['Content-length'] = str(len(content))
    return success_response(handled_headers, content)
