from responses import file_response
from saz.processor import SazProcessor
from torrent.parser import TORRENT_DESCRIPTION_PATTERN, patch_torrent
from utils import BaseHttpHandler, response_to_message


class TorrentPatchHandler(BaseHttpHandler):

    def __init__(self, announcers_provider, announce_listeners=None):
        super(TorrentPatchHandler, self).__init__()
        self.announcers_provider = announcers_provider
        self.announce_listeners = announce_listeners

    def can_observe_flow(self, can_observe_flow):
        return False

    def can_handle_request(self, request):
        return False

    def can_handle_response(self, response):
        self.encoding = BaseHttpHandler.extract_encoding(response)
        if not BaseHttpHandler.is_encoding_supported(self.encoding):
            return False
        if b'content-disposition' in response.headers:
            description = response.headers[b'content-disposition'][1]
            return TORRENT_DESCRIPTION_PATTERN.match(description)

    def handle_response(self, response):
        body = BaseHttpHandler.decode_content_body(response.body, self.encoding)
        with self.announcers_provider() as announcers:
            new_body = patch_torrent(body, announcers, self.announce_listeners)
            encoded_body = BaseHttpHandler.encode_content_body(new_body, self.encoding)
            response.headers[b'content-length'] = (b'content-length', str(len(encoded_body)))
            response.body = encoded_body
            return response.build_response()


class InfoHandler(BaseHttpHandler):
    SUPPORTED_PATH = ['config', 'saz']

    def __init__(self, config_file_provider):
        super(InfoHandler, self).__init__()
        self.req_logger = SazProcessor()
        self.config_file_provider = config_file_provider

    def can_observe_flow(self, can_observe_flow):
        return True

    def observe_request(self, request_id, request):
        self.req_logger.add_request_data(request_id, request.build(include_host=True))

    def observe_response(self, request_id, response):
        self.req_logger.add_response_data(request_id, response.build_response())

    def can_handle_response(self, response):
        return False

    def can_handle_request(self, request):
        url = request.url
        if url.scheme == 'http' and url.netloc == 'smproxy.com' and url.path[1:] in InfoHandler.SUPPORTED_PATH:
            return True

    def handle_request(self, request):
        ulr_path = request.url.path[1:]
        if ulr_path == 'config':
            with self.config_file_provider() as config_file:
                return response_to_message(config_file.config_request_handler())
        elif ulr_path == 'saz':
            content = self.req_logger.create_saz_file()
            return response_to_message(file_response('debug.saz', content))
