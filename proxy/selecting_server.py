from proxy import Server
import logging
import socket

logger = logging.getLogger(__name__)


def build_selecting_server_cls(config_file_provider):
    class SelectingServer(Server):

        def connect(self):
            with config_file_provider() as config_file:
                self.proxy_config = config_file.pac_function(self.request.url.netloc)
                if self.proxy_config:
                    self.conn = socket.create_connection((self.proxy_config.host, self.proxy_config.port))
                else:
                    super(SelectingServer, self).connect()

        def queue_request(self):
            if self.proxy_config:
                logger.info("{id} -> Serving: {url}, using proxy: {proxy_host}:{proxy_port}".format(
                    id=self.request_id,
                    url=self.request.build_url(include_host=True),
                    proxy_host=self.proxy_config.host,
                    proxy_port=self.proxy_config.port
                ))
                if self.request.method == b'CONNECT':
                    # for http connect methods (https requests)
                    # queue appropriate response for client
                    # notifying about established connection
                    raw_request = self.request.build(
                        del_headers=[b'proxy-authorization', b'connection', b'keep-alive'],
                        add_headers=[],
                        include_host=True)
                    logger.debug("%s -> Sending to server: %s" % (self.request_id, raw_request))
                    self.queue(raw_request)
                else:
                    # for usual http requests, re-build request packet
                    # and queue for the server with appropriate headers
                    raw_request = self.request.build(
                        del_headers=[b'proxy-authorization', b'proxy-connection', b'connection', b'keep-alive'],
                        add_headers=[(b'Connection', b'Close')],
                        include_host=True)
                    logger.debug("%s -> Sending to server: %s" % (self.request_id, raw_request))
                    self.queue(raw_request)
            else:
                super(SelectingServer, self).queue_request()

    return SelectingServer
