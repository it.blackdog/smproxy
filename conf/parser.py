# -*- coding: utf-8 -*-
import re

from conf import ProxyConfig
from abc import ABCMeta, abstractmethod

from proxy.responses import text_response

DEFAULT_PROXY_RE = re.compile(r'^DEFAULT POXY:\s*(.+?):(\d+)\s*$', flags=re.IGNORECASE)

DEFAULT_LINE_RE = re.compile(r'^(.+?)(;(.+?):(\d+))?\s*$', flags=re.IGNORECASE)

COMMENT_RE = re.compile(r'\s*#.+$')


class AbsConfig(object):
    """Abstract class for all Proxy Configuration classes
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def pac_function(self, host):
        """PAC function implementation

        :param host: Host to return ProxyConfig for
        :type host: str

        :return: None if Proxy Configuration is not found for given host, or ProxyConfig class instance for host
        :rtype: ProxyConfig|None
        """
        pass

    @abstractmethod
    def get_config_lines(self):
        """
        Generate List of lines that represent current config
        (Returned data can be stored in file and config could be restored from this file)
        :return: List of config lines
        :rtype: list[str]
        """
        pass

    def config_request_handler(self):
        """
        Handling HTTP request and return current config in plain text

        :return: Current config as plain text
        """
        return text_response('\r\n'.join(self.get_config_lines()))


class ConfigFile(AbsConfig):
    """Simple Dict based AbsConfig implementation
    """

    def __init__(self, hosts_dict, default_proxy=None):
        """

        :param hosts_dict: Dictionary for pac function implementation
        :type hosts_dict: dict[str, ProxyConfig]
        :param default_proxy: Default ProxyConfig used for `get_config_lines` implementation
        :type default_proxy: ProxyConfig
        """
        self.hosts_dict = hosts_dict
        self.default_proxy = default_proxy

    def pac_function(self, host):
        lower_host = host.lower()
        for config_host, config_value in self.hosts_dict.iteritems():
            if lower_host.endswith(config_host):
                return config_value

    def get_config_lines(self):
        result = []
        if self.default_proxy:
            result.append('DEFAULT POXY: %s:%s' % (self.default_proxy.host, self.default_proxy.port))
        for key, value in self.hosts_dict.iteritems():
            line = key
            if value != self.default_proxy:
                line += " %s:%s" % (value.host, value.port)
            result.append(line)
        return result


def parse_config(config_iter):
    """Parse ConfigFile from input (string iter), honoring Config File Format

    Example config file:

    `
    DEFAULT POXY: some.proxy.com:3128 # Comment
    some-host.net
    another-host.com antother-proxy.con:8080
    `

    :param config_iter: Iterable string source
    :type config_iter: list[str]
    :return: ConfigFile instance, constructed with data from input
    :rtype: ConfigFile
    """
    default_proxy = None
    config_dict = {}

    comments_removed = map(lambda str: COMMENT_RE.sub('', str), config_iter)
    comments_removed = filter(lambda str: str.strip() != '', comments_removed)

    for idx, line in enumerate(comments_removed):
        if idx == 0:
            math = DEFAULT_PROXY_RE.match(line)
            if math:
                default_proxy = ProxyConfig(math.group(1), int(math.group(2)))
                continue
        math = DEFAULT_LINE_RE.match(line)
        if not math:
            raise Exception("Config file line does not math format (%s)" % line)
        host = math.group(1).lower()
        host_proxy = default_proxy
        if math.group(2):
            host_proxy = ProxyConfig(math.group(3), int(math.group(4)))
        elif default_proxy is None:
            raise Exception("Config file line does not contains 'DEFAULT POXY:' directive (%s)" % line)

        config_dict[host] = host_proxy

    if len(config_dict) == 0:
        raise Exception("Config file does not math format: no proxy config line found")

    return ConfigFile(config_dict, default_proxy)
