# -*- coding: utf-8 -*-
from collections import namedtuple

ProxyConfig = namedtuple('ProxyConfig', ['host', 'port'])

TimedValue = namedtuple('TimedValue', ['proxy_config', 'valid_until'])
