# -*- coding: utf-8 -*-
import contextlib
import functools
import urllib2
from abc import ABCMeta, abstractmethod, abstractproperty
from datetime import datetime


class RetryException(Exception):
    """
    Support class for `retrying_wrapper` for hold raised exceptions during wrapped function execution.
    """

    def __init__(self, errors):
        super(RetryException, self).__init__('Maximal retries number reached')
        self.errors = errors


def retrying_wrapper(func, times, exception_classes):
    """
    Wrapper for `@contextlib.contextmanager` which warps target `func` execution in try -- except block
    and if func raises exception from `exception_classes` then function will be re-executed until `fun` yields value or
    maximal execution `times` reached.
    if `func` execution count will exceed allowed `times` then `RetryException` with all errors catched during execution will be raised.
    :param func: target `@contextlib.contextmanager` to wrap
    :param times: maximal `func` execution count when raised exception class in `exception_classes`
    :param exception_classes: expected exception classes array, if raised exception does not contains in this array,
    then exception will be raised to top context
    :return: `@contextlib.contextmanager`
    """

    @functools.wraps(func)
    @contextlib.contextmanager
    def wrap(*args, **kwargs):
        catched = []
        for r in xrange(0, times):
            try:
                with func(*args, **kwargs) as res:
                    yield res
                    return
            except Exception as e:
                if e.__class__ in exception_classes:
                    catched.append(e)
                else:
                    raise
        raise RetryException(catched)

    return wrap


class AbsCacheStore(object):
    """
    Cache Store interface class
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def has_data(self):
        """
        Check current cache state
        :return: True if Cache contains valid data
        """
        return False

    @abstractmethod
    def set_data(self, data):
        """
        Set data in to Cache
        :param data: data to be stored
        """
        return None

    @abstractproperty
    def data(self):
        """
        Get previously stored data
        :return: data from cache
        """
        return None


class MemoryStore(AbsCacheStore):
    """
    Cache Store that simply store received data into memory for all future invocations
    """

    def __init__(self):
        self.inner_data = None

    def has_data(self):
        return self.data is not None

    def set_data(self, data):
        self.inner_data = data

    @property
    def data(self):
        return self.inner_data


class TimedStore(AbsCacheStore):
    """
    Cache Store that invalidate cache status after `ttl` `timedelta`
    Storing will be done using `parent_store` `AbsCacheStore`
    """

    def __init__(self, parent_store, ttl):
        self.parent_store = parent_store
        self.ttl = ttl
        self.set_at = None

    def has_data(self):
        return self.parent_store.has_data() and self.set_at + self.ttl > datetime.utcnow()

    def set_data(self, data):
        self.parent_store.set_data(data)
        self.set_at = datetime.utcnow()

    @property
    def data(self):
        return self.parent_store.data


def timed_memory_store(ttl):
    """Helper function for adopt `TimedStore` to `cached_data` usage"""
    return lambda: TimedStore(MemoryStore(), ttl)


def cached_data(func, cache_store=MemoryStore):
    """
    Wrapper for `@contextlib.contextmanager` used to cache returned value from `func` using `cache_store`

    :param func: `@contextlib.contextmanager` function witch will be wrapped
    :param cache_store: Cache store implementation class
    :return: `@contextlib.contextmanager` which will return value from `func` and cache it for future calls according to
    provided `cache_store` class
    """

    @functools.wraps(func)
    @contextlib.contextmanager
    def wrap(*args, **kwargs):
        if wrap.__cache.has_data():
            yield wrap.__cache.data
            return
        else:
            with func(*args, **kwargs) as res:
                wrap.__cache.set_data(res)
                yield res
                return

    wrap.__cache = cache_store()

    return wrap


@contextlib.contextmanager
def file_provider(file_name):
    """
    Read file, and yields file content as list[str]

    :param file_name: File to read
    :type file_name: str
    :return: Generator of list[str]
    """
    with open(file_name, 'r') as f:
        yield f.read().splitlines()


def file_input(file_name):
    """
    Decorator for `file_provider` with `file_name` parameter

    :param file_name: File name
    :type file_name: str
    :return: Decorated `file_provider`
    """

    def file_input_dec(func):
        @functools.wraps(func)
        def wrapper(*_, **kwargs):
            with file_provider(file_name) as content:
                args = [content]
                return func(*args, **kwargs)

        return wrapper

    return file_input_dec


@contextlib.contextmanager
def url_provider(url):
    """
    Read content form given URL yields file content as list[str]

    :raise urllib2.HTTPError if URL return code not 200

    :param url: URL to fetch config
    :type url: str
    :return: Generator of list[str]
    """
    response = urllib2.urlopen(url)
    if response.code != 200:
        raise urllib2.HTTPError(response.url, response.code, "Unexpected return code",
                                response.headers, None)

    yield response.read()
