# -*- coding: utf-8 -*-
import contextlib
import os
import unittest
from datetime import timedelta
from time import sleep

from conf import ProxyConfig
from conf.parser import parse_config, ConfigFile
from conf.providers import file_provider, retrying_wrapper, RetryException, cached_data, timed_memory_store
from tutils import create_temp_file, counted


class TestParseConfig(unittest.TestCase):

    def test_should_parse_valid(self):
        result = parse_config(
            ['# Comment string', 'DEFAULT POXY: proxy1.com:8081', 'banned1.com', 'banned2.com6532;proxy2.com:8082'])
        self.assertIsInstance(result, ConfigFile)
        self.assertEqual(result.default_proxy, ProxyConfig('proxy1.com', 8081))

    def test_should_parse_no_default(self):
        result = parse_config(['banned1.com;proxy1.com:8081', 'banned2.com;proxy2.com:8082'])
        self.assertIsInstance(result, ConfigFile)
        self.assertIsNone(result.default_proxy)

    def test_fail_without_default(self):
        with self.assertRaises(Exception):
            parse_config(['banned1.com'])

    def test_parsed_function(self):
        result = parse_config(['DEFAULT POXY: proxy1.com:8081 # commnet', 'banned1.com  # commnet',
                               'banned2.com:6532;proxy2.com:8082 # commnet'])
        pac_function = result.pac_function
        self.assertEqual(pac_function('banned1.com'), ProxyConfig('proxy1.com', 8081))
        self.assertEqual(pac_function('banned2.com:6532'), ProxyConfig('proxy2.com', 8082))


class TestProviders(unittest.TestCase):
    def test_file_provider(self):
        input_content = ['line1', 'line2']
        with create_temp_file(os.linesep.join(input_content)) as tempfile:
            with file_provider(tempfile) as content:
                self.assertEqual(input_content, content)


class TestCachedData(unittest.TestCase):

    def test_always_return_cached(self):
        @counted
        @contextlib.contextmanager
        def ct():
            if ct._call_counter > 1:
                yield 2
            else:
                yield 1

        provider = cached_data(ct)
        with provider() as first_time:
            self.assertEquals(first_time, 1)

        with provider() as second_time:
            self.assertEquals(second_time, 1)

        self.assertEquals(ct._call_counter, 1)

    def test_args_passing(self):
        @counted
        @contextlib.contextmanager
        def ct(*provided_args):
            yield provided_args

        provider = cached_data(ct)
        args = [10, True, 'string']
        with provider(*args) as result:
            self.assertItemsEqual(result, args)

    def test_timed_cache(self):
        @counted
        @contextlib.contextmanager
        def ct():
            if ct._call_counter > 1:
                yield 2
            else:
                yield 1

        provider = cached_data(ct, timed_memory_store(timedelta(microseconds=100)))
        with provider() as first_time:
            self.assertEquals(first_time, 1)

        with provider() as second_time:
            self.assertEquals(second_time, 1)

        sleep(0.1)
        with provider() as third_time:
            self.assertEquals(third_time, 2)

        self.assertEquals(ct._call_counter, 2)


class TestRetryingWrapper(unittest.TestCase):

    def test_args_passing(self):
        @counted
        @contextlib.contextmanager
        def cm(*supplied_args):
            yield supplied_args

        provider = retrying_wrapper(cm, 3, [Exception])
        args = [10, True, 'string']
        with provider(*args) as result:
            self.assertItemsEqual(result, args)

    def test_normal_return(self):
        @counted
        @contextlib.contextmanager
        def cm():
            yield 1

        provider = retrying_wrapper(cm, 3, [Exception])
        with provider() as result:
            self.assertEquals(result, 1)

        self.assertEquals(cm._call_counter, 1)

    def test_no_return_all_errors(self):
        @counted
        @contextlib.contextmanager
        def all_errors():
            raise IOError('done')

        provider = retrying_wrapper(all_errors, 3, [IOError])

        with self.assertRaises(RetryException) as rh:
            with provider() as _:
                self.assertFalse(True, "must not be called")

        self.assertEquals(len(rh.exception.errors), 3)

        self.assertEquals(all_errors._call_counter, 3)

    def test_return_after_failures(self):
        @counted
        @contextlib.contextmanager
        def success_after_two_failures():
            if success_after_two_failures._call_counter < 2:
                raise IOError('done')
            yield 1

        provider = retrying_wrapper(success_after_two_failures, 3, [IOError])

        with provider() as result:
            self.assertEquals(result, 1)

        self.assertEquals(success_after_two_failures._call_counter, 2)

    def test_fail_on_unexpected(self):
        @counted
        @contextlib.contextmanager
        def throw_unexpected():
            if throw_unexpected._call_counter == 1:
                raise IOError('done')
            elif throw_unexpected._call_counter == 2:
                raise ArithmeticError('unexpected')
            yield 1

        provider = retrying_wrapper(throw_unexpected, 3, [IOError])
        with self.assertRaises(ArithmeticError):
            with provider() as _:
                self.assertFalse(True, "must not be called")

        self.assertEquals(throw_unexpected._call_counter, 2)


if __name__ == '__main__':
    unittest.main()
