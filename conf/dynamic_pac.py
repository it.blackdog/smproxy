# -*- coding: utf-8 -*-

import time
from datetime import timedelta

from conf import TimedValue
from conf.parser import AbsConfig

DEFAULT_TTL = timedelta(hours=5).total_seconds()


class DynamicPac(AbsConfig):
    """Proxy Configuration implementation with ability to dynamically add hosts

    :argument base_config_file: Base proxy configuration
    :type base_config_file: AbsConfig
    """

    def __init__(self, base_config_file):
        self.dynamic_hosts = {}

        self.__base_config_file = base_config_file

    def pac_function(self, host):
        result = self.__base_config_file.pac_function(host)
        if result is None:
            result = self._get_dynamic_host(host)
        return result

    def _get_dynamic_host(self, host):
        """ Check if dynamic configuration for given host

        :param host: host to check
        :type host: str
        :return: if ProxyConfig config exits and it is not expired ProxyConfig returns, None otherwise
        :rtype: conf.ProxyConfig|None
        """
        ttl_result = self.dynamic_hosts.get(host, None)
        if ttl_result:
            if ttl_result.valid_until > time.time():
                return ttl_result.proxy_config
            else:
                del self.dynamic_hosts[host]

    def add_dynamic_host(self, host, proxy_config):
        """Add new host to current configuration

        Added host wil be available in DEFAULT_TTL seconds

        :param host: new host
        :type host: str
        :param proxy_config: proxy that will be used to serve host
        :type proxy_config: conf.ProxyConfig
        :return: Nothing
        """
        self.dynamic_hosts[host] = TimedValue(proxy_config, time.time() + DEFAULT_TTL)

    def get_config_lines(self):
        lines = self.__base_config_file.get_config_lines()
        now = time.time()
        for host, ttl_result in self.dynamic_hosts.iteritems():
            if ttl_result.valid_until > now:
                lines.append("%s # Valid until: %s" %
                             (host,
                              time.strftime("%Y-%m-%d %H:%M:%S %Z", time.localtime(ttl_result.valid_until)))
                             )
        return lines
