# -*- coding: utf-8 -*-
import contextlib
import functools
import os
import tempfile
import unittest
from collections import namedtuple

CallAttempt = namedtuple('CallAttempt', ['args', 'result'])


@contextlib.contextmanager
def create_temp_file(content):
    (fd, temp_name) = tempfile.mkstemp(text=True)
    os.write(fd, content)
    os.close(fd)
    try:
        yield temp_name
    finally:
        try:
            os.close(fd)
        except Exception:
            pass
        os.unlink(temp_name)


class ExtendedCase(unittest.TestCase):

    def assertMethodCalled(self, expected_attempts):
        call_iter = iter(expected_attempts)

        def call_method(*called_args):
            attempt = call_iter.next()
            self.assertEquals(attempt.args, called_args)
            return attempt.result

        return call_method


def counted(func):
    @functools.wraps(func)
    def wrp(*args, **kwargs):
        wrp._call_counter += 1
        return func(*args, **kwargs)

    wrp._call_counter = 0

    return wrp
