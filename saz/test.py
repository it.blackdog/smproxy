# -*- coding: utf-8 -*-
import unittest

from saz.utils import LimitedDict


class TestLimitedDic(unittest.TestCase):

    def test_init(self):
        dict = LimitedDict(2)
        self.assertIsNotNone(dict)

    def test_limit(self):
        dict = LimitedDict(2)
        dict[1] = 1
        dict[2] = 2
        self.assertDictEqual(dict, {1: 1, 2: 2})

        dict[3] = 3
        self.assertDictEqual(dict, {2: 2, 3: 3})

        dict[2] = 5
        self.assertDictEqual(dict, {2: 5, 3: 3})

    def test_default_item(self):
        dict = LimitedDict(2, lambda: 'default')
        self.assertFalse(1 in dict)
        self.assertEquals(dict[1], 'default')


if __name__ == '__main__':
    unittest.main()
