# -*- coding: utf-8 -*-
import StringIO
import sys
import zipfile
from time import time

from saz.utils import LimitedDict, RequestStat, DataContainer


class SazProcessor(object):
    """Save request/response data for logging and inspections. Currently only stores last 100 request."""
    data = LimitedDict(100)

    def add_request_data(self, request_id, request_data):
        """Save request data portion(could be called multiple times for single request_id)
        :param request_id:  unique request id
        :type request_id: str
        :param request_data: raw request data portion
        :type request_data: str
        :return: None
        """
        stat = self.data.get(request_id, None)
        if not stat:
            stat = RequestStat(request=DataContainer(data=[], timestamp=time()), response=None)
            self.data[request_id] = stat
        stat.request.data.append(request_data)

    def add_response_data(self, request_id, response_data):
        """Save response data portion(could be called multiple times for single request_id)
        :param request_id:  unique request id
        :type request_id: str
        :param response_data: raw response data portion
        :type response_data: str
        :return: None
        """
        stat = self.data.get(request_id, None)
        if stat:
            if not stat.response:
                stat.response = DataContainer(data=[], timestamp=time())
            stat.response.data.append(response_data)

    def create_saz_file(self):
        """Generate SAZ file for request/response inspection using Fiddler
        :rtype:str
        """
        output = StringIO.StringIO()
        with zipfile.ZipFile(output, mode='w') as zf:
            zf.writestr('[Content_Types].xml', """<?xml version="1.0" encoding="utf-8" ?>
<Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types">
<Default Extension="htm" ContentType="text/html" />
<Default Extension="xml" ContentType="application/xml" />
<Default Extension="txt" ContentType="text/plain" />
</Types>""")
            stat_items = filter(lambda stat: stat.response, self.data.itervalues())
            for (index, stat) in enumerate(stat_items):
                prefix = "raw/%s_" % (index + 1)
                zf.writestr(prefix + 'c.txt', ''.join(stat.request.data))
                zf.writestr(prefix + 's.txt', ''.join(stat.response.data))
        result = output.getvalue()
        output.close()
        return result

    def log_stat(self, request_id):
        """Log request data by request_id
        :param request_id: request id to write log
        :return: None
        """
        stat = self.data.get(request_id, None)
        if stat:
            request = stat.request
            sys.stdout.write("\r\n%s >>> %s\r\n" % (request_id, request.timestamp))
            for line in request.data:
                sys.stdout.write("%s >>> %s" % (request_id, line))

            response = stat.response
            if response:
                sys.stdout.write("%s <<< %s\r\n" % (request_id, response.timestamp))
                for line in response.data:
                    sys.stdout.write("%s <<< %s" % (request_id, line))
            else:
                # TODO: strange cases for request without response
                sys.stdout.write("%s !!!!!!!!!!!!!!!!!!!!!!!\r\n" % request_id)
