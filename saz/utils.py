# -*- coding: utf-8 -*-
from collections import OrderedDict, namedtuple

DataContainer = namedtuple('DataContainer', ['data', 'timestamp'])


class RequestStat(object):
    """
    Request static holder
    """

    def __init__(self, request=None, response=None):
        """
        :param request: Request DataContainer
        :param response: Response DataContainer
        """
        self.request = request
        self.response = response


class LimitedDict(OrderedDict):
    """Dict implementation with limited capacity, and optional `default_item` as producer for items that does not exist in Dict
    """

    def __init__(self, maxlen, default_item=None):
        """Constructor

        :param maxlen: Maximal Dict capacity
        :type maxlen: int
        :param default_item: Optional builder for return at key that does not found in Dict
        """
        super(LimitedDict, self).__init__()
        self.maxlen = maxlen
        self.default_item = default_item

    def __getitem__(self, k):
        """
        Get data from dict or create it from `default_item` if set
        :param k: key to find data
        :return: dict value by key or result of `default_item` if present
        """
        if not self.default_item:
            return super(LimitedDict, self).__getitem__(k)
        result = super(LimitedDict, self).get(k, None)
        if not result and self.default_item:
            result = self.default_item()
        return result

    def __setitem__(self, k, v):
        """Set value to dict by key
        If dict size > `max_len` then old items will be removed
        :param k: key
        :param v: value
        :return:
        """
        if not self.__contains__(k):
            for key in self.iterkeys():
                if self.__len__() < self.maxlen:
                    break
                self.__delitem__(key)
        super(LimitedDict, self).__setitem__(k, v)
