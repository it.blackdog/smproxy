# -*- coding: utf-8 -*-
import unittest

import bencode
from torrent.parser import patch_torrent
from tutils import ExtendedCase, CallAttempt

EXAMPLE_TORRENT_DICT = {
    'announce': 'udp://some-announce.org:2710',
    'announce-list': [
        ['http://announce1.com', 'http://announce2.com'],
        ['http://announce3.com']
    ]
}

EXAMPLE_ANNOUNCERS_TO_ADD = ['http://new-announce1.com', 'http://new-announce2.com']

EXAMPLE_TORRENT_CONTENT = bencode.encode(EXAMPLE_TORRENT_DICT)


class TestTorrentFilePatcher(ExtendedCase):

    def test_added_new_announcers(self):
        result = patch_torrent(EXAMPLE_TORRENT_CONTENT, EXAMPLE_ANNOUNCERS_TO_ADD)
        result_dict = bencode.decode(result)

        for new_announcer in EXAMPLE_ANNOUNCERS_TO_ADD:
            self.assertIn([new_announcer], result_dict['announce-list'])

        for old_announcers in EXAMPLE_TORRENT_DICT['announce-list']:
            self.assertIn(old_announcers, result_dict['announce-list'])

    def test_parsed_announcers_notification(self):
        watcher = self.assertMethodCalled([CallAttempt(
            args=(['udp://some-announce.org:2710', 'http://new-announce1.com', 'http://new-announce2.com',
                   'http://announce1.com', 'http://announce2.com', 'http://announce3.com'],), result=None)])
        patch_torrent(EXAMPLE_TORRENT_CONTENT, EXAMPLE_ANNOUNCERS_TO_ADD, watcher)


if __name__ == '__main__':
    unittest.main()
