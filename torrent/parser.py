# -*- coding: utf-8 -*-
import bencode
import re

# 'attachment; filename="[rutor.is]2001_god_-_Kosmicheskaja_odisseja_1968_Remaster.torrent"'
TORRENT_DESCRIPTION_PATTERN = re.compile(r'.+?filename=".+?\.torrent"')


def patch_torrent(encoded_content, announcers, announce_listeners=None):
    """Decode .torrent file, return patched .torrent with added announcers list

    :param encoded_content: raw .torrent file content
    :type encoded_content: str
    :param announcers: list of announcers that will be added in the beginning of .torrent file 'announce-list' section
    :type announcers: list[str]
    :param announce_listeners: optional callback that wil be called on .torrent file parsing with original content of
    'announce' and 'announce-list' section
    :type announce_listeners: str -> None
    :return: patched .torrent file content
    :rtype: str
    """
    content = bencode.decode(encoded_content)
    ann_list = content.get('announce-list', [])
    external_announcers = [[announcer] for announcer in announcers]
    new_announcers = list(external_announcers) + list(ann_list)
    content['announce-list'] = new_announcers

    if announce_listeners:
        main_announce = content.get('announce')
        main_announce = [main_announce] if main_announce else []
        flat_announcers = []
        for announcers_list in new_announcers:
            flat_announcers = flat_announcers + announcers_list
        announce_listeners(main_announce + flat_announcers)

    return bencode.encode(content)


def build_response_handler(announcers, announce_listeners=None):
    """Function that create HttpResponse filter
    If response contains .torrent file, this file will be patched with `patch_torrent` function

    :param announcers: list of announcers that will be added in the beginning of .torrent file 'announce-list' section
    :type announcers: list[str]
    :param announce_listeners: optional callback that wil be called on .torrent file parsing with original content of
    'announce' and 'announce-list' section
    :type announce_listeners: str -> None
    :return: Function that handle HttpResponse
    """

    def response_handler(req, req_body, res, res_body):
        if 'Content-Disposition' in res.headers:
            description = res.headers['Content-Disposition']
            match = TORRENT_DESCRIPTION_PATTERN.match(description)
            if match:
                return patch_torrent(res_body, announcers, announce_listeners)
        return None

    return response_handler
